//Control
function sesionControl(link,page_name,page_pos){
	var sesion=getSesion(link);
	if(sesion && page_name=='index_out')	window.location.assign('user/');
	if(!sesion && page_name=='index_in')	window.location.assign('../');
	if(!sesion && page_name=='hacerNoticia')	window.location.assign('../');
	if(!sesion && page_name=='configuracion')	window.location.assign('../');
}

//Redirecciones
function redirectionLogo(){
	var pos = document.getElementById("page_pos").value;
    var pag_nombre = document.getElementById("page_name").value;
    var url=linked(pos);
    var seccion = getSesion(url);

    if(pos==0){
        url='./';
    }else{
        if(seccion){
            if(pag_nombre=="hacerpregunta" || pag_nombre=="pregunta"){
                url=url+"user";
            }else{
                pos--;
                url=linked(pos);
            }
        }
    }
    window.location.assign(url);
}

function redirection(){
	var win=window.open("https://twitter.com/?lang=es", '_blank');
	win.focus();
}

function redirectionNewPost(){
	var win=window.open("posts/post.html", '_self');
}

//Sets
function setLogin(){
	
	var name=document.getElementById('id-input-email').value;
	var pass=document.getElementById('id-input-password').value;
	var option="setSesion";
	
	
	$.ajax({
		beforeSend: function(){
		
		},
		type: 'POST',
		data: {name:name,pass:pass,option:option},
		url:'php/switch/switch.set.php',
		dataType:'json',
		
		success: function(datos,textStatus,jqXHR){
			if(datos.error){
				alert(datos.mensaje);
			}else{
				console.log(datos.redirect);
				//window.location= "index.html";
				window.location.assign(datos.redirect);
			}
		},
		error: function(jqXHR,estado,error){
			console.log("Estado: "+estado);
			console.log("Error: "+error);
		},
		complete: function(jqXHR,estado){
			console.log("Se completo la tarea: "+estado);
		}
	});
}

//Gets
function getSesion(link){
	var id=0;
	var option="getSesion";
	$.ajax({
		type: "POST",
		data: {option:option},
		url: link+"php/switch/switch.set.php",
		dataType: 'json',
		succes: function(datos, estado, jqXHR){
			id=datos;
		},
		error: function(jqXHR,estado,error){
			console.log("Estado: "+estado);
			console.log("Error: "+error);
		}
	});

	return id;
}

//Extras
function linked(pos){
    var url = '';
    var i;
    for(i=0;i<pos;i++){
        url='../'+url;
    }
    return url;
}


