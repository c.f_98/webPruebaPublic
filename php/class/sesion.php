<?php
	class Sesion{
		private $conect;

		public function __construct($con){
			$this->conect=$con;
		}

		public function init(){
			@session_start();
		}

		public function set($name, $value){
			$_SESSION[$name]=$value;
		}

		public function get(){
			$idSesion;
			if(isset($_SESSION['user_id'])){
				$idSesion=$_SESSION['user_id'];
			}else{
				$idSesion=0;
			}
			return $idSesion;
		}
	}
	
?>