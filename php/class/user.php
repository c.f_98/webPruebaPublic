<?php
	class User{
		private $conect;

		//Conexion a base de datos
		public function __construct($con){
			$this->conect=$con;
		}

		//Login
		public function setSesion($nick, $pass, $sesion){
			$datos=array("error"=>false, "mensaje"=>"");

			//Verifico que haya ingresado mail y contraseña
			if($nick!='' && $pass!=''){
				$passEncript=sha1($pass);
				$consulta="SELECT * FROM users WHERE email='$nick' AND password='$passEncript'";
				$result=mysqli_query($this->conect,$consulta);
				$contRows=mysqli_num_rows($result);

				if($contRows==1){

					$user=mysqli_fetch_array($result);
					//Variables generales para el usuario
					$sesion->set('user_name', $user['name']);
					$sesion->set('user_id', $user['id']);
					$sesion->set('user_email',$user['email']);

					$id=$user['id'];

					$consulta="SELECT * FROM online WHERE online_id_user='$id'";
					$result=mysqli_query($this->conect,$consulta);
					$contRows=mysqli_num_rows($result);
					
					//Registro en linea al usuario
					if($contRows==0){
						$dateIngreso=date("Y-n-j H:i:s");
						$consulta="INSERT INTO online(online_id_user,online_date) 
							VALUES ('$id','$dateIngreso')";
						$result=mysqli_query($this->conect,$consulta);
					}
					$datos['redirect']="./index.html";
					
				}else{
					$datos=["error"=>true, "mensaje"=>"Usuario o contraseña incorrecta"];
				}
			}else{
				$datos=["error"=>true, "mensaje"=>"Campos vacios"];
			}

			return $datos;
		}
	}
	
	
?>